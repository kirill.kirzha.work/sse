// pages/api/stream.js

export default function (req, res)  {
    res.setHeader('Content-Type', 'text/event-stream');
    res.setHeader('Cache-Control', 'no-cache');
    res.setHeader('Connection', 'keep-alive');

    let counter = 0;

    const sendEvent = (message) => {
        res.write(message);
        counter++;
    };

    sendEvent('Initial Event');

    const intervalId = setInterval(() => {
        sendEvent(`Event ${counter}`);
    }, 5000);

    req.on('close', () => {
        clearInterval(intervalId);
        res.end();
    });
};



