"use client"
import {fetchEventSource} from "@ai-zen/node-fetch-event-source";
import React, { useState } from 'react';

const Home = () => {
    const [events, setEvents] = useState([]);
    const [message, setMessage] = useState('')

        const ctrl = new AbortController();
        fetchEventSource('/api/stream', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                messages: message
            }),
            onmessage: (ev) => {
                console.log(ev)
            },
            signal: ctrl.signal,
        }).then((ev) => {
            console.log(ev)
        })

    return (
        <div>
            <h1>Event Streaming</h1>
            <input onChange={e => setMessage(e.target.value)}/>
        </div>
);
};

export default Home;
